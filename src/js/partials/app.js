$(document).ready(function(){
    $('.js-change-txt').each(function(){
        var $this = $(this),
        $txt = $this.find('.js-change-txt-span'),
        $form = $this.find('form'),
        $input = $form.find('input[type="text"]');
        $form.submit(function(){
            $txt.text($input.val());
            return false;
        });
    });

    $('.widget').each(function(e){

        var $this = $(this),
        $remove = $this.find('.js-btn-panel-remove');
        $remove.on('click', function(e){
            $this.addClass('remove').append('<div class="panel-remove"><div class="panel-remove__cont">Виджет удален <br> <a href="#" class="js-remove-back">Отмена</a></div></div>');
        })
    });

    $('body').on('click', '.js-remove-back', function(){
        var $this = $(this).closest('.panel'),
        $panelRemove = $this.find('.panel-remove');
        $panelRemove.remove();
        $this.removeClass('remove');
    });

    $('#navbar-minimalize').on('click', function(){
        var $navbar = $('body');
        $navbar.toggleClass('close-navbar open-navbar');
    });
    $('.navbar-wrapp').on('click', function(e){
        $('body').toggleClass('close-navbar open-navbar');
    });
    $('#navbar').on('click', function(e){
        e.stopPropagation();
    });
    $(window).on('resize', function(){
        var $this = $(this);
        if($this.width()< 1220) {
            $('body').addClass('close-navbar mobile-navbar');
        } else {
            $('body').removeClass('close-navbar mobile-navbar');
        }

    });

    // CHOSEN SELECT
    $('.js-select').chosen({
        disable_search_threshold: 10
    });

    // DATE PICKER
    var datepickerRU = {
        months:['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        weekdays:['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
    }
    var $datePickElement = $('.js-datepicker');
    $datePickElement.each(function(){
        var $this = $(this);
        var datepicker = UIkit.datepicker($this, {
            i18n: datepickerRU,
            format:'DD.MM.YYYY'
        });
    });

    var $datePickStart = $('#js-datepicker-start');
    var $datePickEnd = $('#js-datepicker-end');


    var datepickerStart = UIkit.datepicker($datePickStart, {
        i18n: datepickerRU,
        format:'DD.MM.YYYY',
        // minDate: 0 // минимум текущее число
    });
    var datepickerEnd = UIkit.datepicker($datePickEnd, {
        i18n: datepickerRU,
        format:'DD.MM.YYYY',
        // minDate: 0
    });
    datepickerStart.on('hide.uk.datepicker', function (){
        datepickerEnd.options.minDate = UIkit.Utils.moment($(this).val(), "DD.MM.YYYY").format('DD.MM.YYYY');
        datepickerEnd.update();
    });
    datepickerEnd.on('hide.uk.datepicker', function(){
        datepickerStart.options.maxDate = UIkit.Utils.moment($(this).val(), "DD.MM.YYYY").format('DD.MM.YYYY');
        datepickerStart.update();
    });

    //SOCIAL BIND
    $('body').on('click', '.social-bind__link', function(e){
        var $this = $(this);
        $this.toggleClass('active');
        e.stopPropagation();
        e.preventDefault();
    });

    //BY COMBINING
    var byCombiming = function(){
        var $byCombiningSelected = $('#by-combining-selected'),
            $byCombiningSelectedName = $('#form-by-combining__num'),
            $byCombiningAdd = $('#by-combining-add'),
            $byCombininRadio = $byCombiningSelected.find('input[type="radio"]'),
            $byCombininCheck = $byCombiningAdd.find('input[type="checkbox"]'),
            $byCombiningAddRow = $byCombiningAdd.find('.uk-form-row');

            $byCombiningSelected.on('change', 'input', function(){
                var $this = $(this),
                    $selNum = $this.data('sel');
                    $('.by-combining .invisible').removeClass('invisible');
                    $byCombiningAddRow.removeClass('invisible');
                    $byCombininCheck.prop('checked',false);
                    $byCombiningAddRow.eq($selNum).addClass('invisible');
            });
    }
    byCombiming();


        var $headDropdown = $(".header .uk-button-dropdown");
        $headDropdown.each(function(){
            var $this = $(this);
            var headDropdown = UIkit.dropdown($this, {
                mode: 'click',
                boundary: '#page-wrapper',
                pos:'bottom-center'
            });
        });


        //tabserv
        // $("a.tab-serv__item").click(function() {
        //
        //     var elementClick = $(this).attr("href")
        //     var destination = $(elementClick).offset().top;
        //     jQuery("html:not(:animated),body:not(:animated)").animate({
        //       scrollTop: destination
        //     }, 800);
        //     // return false;
        //   });

        if (window.innerWidth > 1220) {
            sliderStiky = UIkit.sticky('.slider-bann', {top: 10, media: 1024, boundary: '.js-stop-banner'});
        }

    //SLIMSCROLL http://rocha.la/jQuery-slimScroll
    $('.widget-scroll').slimScroll({
       height: 'auto',
       alwaysVisible : true,
       color: '#d62631',
       opacity: 1,
       railColor: '#e4e6e8',
    //    railVisible: 'true',
       distance:'0px',
       railOpacity: 1,
       disableFadeOut: true
   });


   //CHAT
   var $chatContainer = $('.chat');
   if ($chatContainer.length > 0) {
       function chatH(){
           return $(window).outerHeight() - $('.header').outerHeight() - $('.page-title').outerHeight();
       }
       $chatContainer.css({
           'min-height' : chatH()
       });

       $(window).on('resize', function(){
           $chatContainer.css({
               'min-height' : chatH()
           });
       });
   }


   //BACK HISTORY
   $('.js-back').on('click', function(e){
       e.preventDefault();
       history.back();
   });

   //SHOW MORE
   $('.js-show-more').on('click', function(e){
      e.preventDefault();
      var $btnMore = $(this),
          $showList = $('.js-show-list'),
          $url = $btnMore.attr('href');
          $.ajax({
              url: $url,
            //   type: "post",
              dataType: 'html',
              context: document.body,
              success: function(data){
                 $showList.append($(data).find('.js-show-list').html());
                  return false;
              }
          });
          return false;
   });

   //FORMCOUNT
  $('input.js-count').on('keypress', function(e) {
      e = e || event;

      if (e.ctrlKey || e.altKey || e.metaKey) return;

      var chr = getChar(e);

      // с null надо осторожно в неравенствах, т.к. например null >= '0' => true!
      // на всякий случай лучше вынести проверку chr == null отдельно
      if (chr == null) return;

      if (chr < '0' || chr > '9') {
        return false;
      }

  });

    function getChar(event) {
      if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode) // IE
      }

      if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which) // остальные
      }

      return null; // специальная клавиша
    }

   var $formCounter = $('.form-counter');
   $formCounter.each(function(){
      var $this = $(this),
      $input = $this.find('input'),
      $val = 0;
      $input.val($val);
      $btnInc = $this.find('.form-counter-inc'),
      $btnDec = $this.find('.form-counter-dec');

      $btnInc.on('click', function(){
          $val = $input.val();
          $val++;
          $input.val($val);
          console.log($val);
      });
      $btnDec.on('click', function(){
          $val = $input.val();
          $val--;
          $val < 0 ? $val = 0 : false;
          $input.val($val);
      })
   });


   //LOADING
   var $loading = '<div class="loading"><i class="uk-icon-spinner uk-icon-spin"></i></div>'
   //TARIFF CHANGE
   var $currTariff = $('#tariff-curr-box'),
   $newTariff = $('#tariff-new-box'),
   $widget = $currTariff.find('.widget');

   $newTariff.css({
       'position' : 'relative'
   });
   $widget.on('click', function(event){
       event = event || window.event;
       var target = event.target || event.srcElement;
       var $this = $(this);
       if ($(target).hasClass('js-show-tariff')) {
           $widget.removeClass('active');
           $this.addClass('active');
           var request = $.ajax({
              url: "../tariff-new-box.html",
              dataType: "html",
              success: function(data) {
                $newTariff.html(' ').append($loading).append($(data));
              },
              complete: function(){
                $newTariff.find('.loading').remove();

                var datepickerRU = {
                    months:['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
                    weekdays:['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
                }
                var $datePickElement = $('.js-datepicker');
                $datePickElement.each(function(){
                    var $this = $(this);
                    var datepicker = UIkit.datepicker($this, {
                        i18n: datepickerRU,
                        format:'DD.MM.YYYY'
                    });
                });


                var destination = $newTariff.offset().top;
                jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 800);
              }
            });
       }

   });


   //MASK INPUT
   $('.js-mask-phone').mask('+7(999) 999-9999');

   //
   $('.list-card__remove-item').on('click', function(){
       var $this = $(this);
       $this.closest('.list-card__item').remove();
   });
});




// VIDEO PLAYER

// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');


tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;


function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        // height: '390',
        // width: '640',
        videoId: 'M7lc1UVf-VE',
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
    $('#player-play').on('click', function(){
        var $this = $(this),
            $overlay = $this.closest('.uk-overlay-panel');
        $overlay.hide();
        event.target.playVideo();
    });
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !done) {
        // setTimeout(stopVideo, 6000);
        done = true;
    }
}

function stopVideo() {
    player.stopVideo();
    alert('1111');
}
